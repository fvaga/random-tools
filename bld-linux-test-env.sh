function usage {
    echo "bld-linux-test-env -e /path/to/linux/test/environment -p /path/to/project"
    exit 1
}

LINUX_TEST_ENV=""
PROJECT=""
while getopts ":e:p:" o; do
    case "${o}" in
        e)
            LINUX_TEST_ENV=${OPTARG}
            ;;
        p)
            PROJECT=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

if [ "${LINUX_TEST_ENV}" = "" ] || [ ! -d ${LINUX_TEST_ENV} ]
then
    echo "Invalid path ${LINUX_TEST_ENV}"
    echo ""
    usage
fi

if [ "${PROJECT}" = "" ] || [ ! -d ${PROJECT} ]
then
    echo "Invalid path ${PROJECT}"
    echo ""
    usage
fi


cd ${PROJECT}
for d in $(ls -d $LINUX_TEST_ENV/*v4*)
do
    make LINUX=$d clean || break
    make LINUX=$d || break
done
