#!/bin/sh
GIT_REPO=git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git

function usage {
    echo "gen-linux-test-env -e /path/to/linux/test/environment"
    exit 1
}

function get_linux_version {
    lst=""

    for min in $(seq 0 19):
    do
        lst="$lst v3.$min"
    done
    for min in $(seq 0 20):
    do
        lst="$lst v4.$min"
    done
    for min in $(echo "0"):
    do
        lst="$lst v5.$min"
    done

    echo $lst
}

function linux_stable_update {
    env=$1
    dst=${env}/linux-stable
    if [ ! -d ${dst} ]
    then
        git clone $GIT_REPO ${dst}
    else
        git -C ${dst} fetch origin
    fi
}

function linux_stable_version_create {
    env=$1
    src=${env}/linux-stable

    for ver in $(get_linux_version)
    do
        version=$(git -C ${src} tag --list | grep $ver | sort -V | tail -n 1)
        dst=${env}/linux-stable-$version
        if [ ! -d ${dst} ]
        then
            git clone -l -s -b $version ${src} ${dst}
        fi
    done
}

function linux_stable_version_defconfig {
    env=$1
    for lnx in $(ls -d ${env}/linux-stable-*)
    do
        make -C ${lnx} defconfig || echo $(date -Iseconds)": Failed to defconfig $lnx" >> ${env}/errors-$(date -I).log
    done
}

function linux_stable_version_prepare {
    env=$1
    for lnx in $(ls -d ${env}/linux-stable-*)
    do
        make -C ${lnx} prepare || echo $(date -Iseconds)": Failed to prepare $lnx" >> ${env}/errors-$(date -I).log
    done
}


# MAIN Starts here

LINUX_TEST_ENV=""
PROJECT=""
VERSIONS=""
while getopts ":e:" o; do
    case "${o}" in
        e)
            LINUX_TEST_ENV=$(readlink -f ${OPTARG})
            ;;
        v)
            VERSIONS="$VERSIONS ${OPTARG}"
            ;;
        *)
            usage
            ;;
    esac
done

if [ "${LINUX_TEST_ENV}" = "" ] || [ ! -d ${LINUX_TEST_ENV} ]
then
    echo "Invalid path ${LINUX_TEST_ENV}"
    echo ""
    usage
fi

linux_stable_update ${LINUX_TEST_ENV}
linux_stable_version_create ${LINUX_TEST_ENV}
linux_stable_version_defconfig ${LINUX_TEST_ENV}
linux_stable_version_prepare ${LINUX_TEST_ENV}
